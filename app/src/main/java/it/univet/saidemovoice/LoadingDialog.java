package it.univet.saidemovoice;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.LayoutInflater;
import android.widget.TextView;

class LoadingDialog {

    Activity activity;
    AlertDialog dialog;

    LoadingDialog(Activity myActivity) {
        activity = myActivity;
    }

    void startLoadingDialog(String activityName) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        LayoutInflater inflater = activity.getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.login_dialog, null));
        builder.setCancelable(false);

        dialog = builder.create();
        dialog.show();

        TextView nameLabel = dialog.findViewById(R.id.nameActivity);
        nameLabel.setText(activityName);
    }

    void dismissDialog() {
        dialog.dismiss();
    }
}
