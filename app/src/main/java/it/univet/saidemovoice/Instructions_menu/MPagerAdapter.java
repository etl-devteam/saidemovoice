package it.univet.saidemovoice.Instructions_menu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import it.univet.visionar.visionarservice.VisionARService;

public class MPagerAdapter extends PagerAdapter {

    private final int[] layouts;
    private final LayoutInflater layoutInflater;

    Context ctx;
    VisionARService myService = null;

    public MPagerAdapter(int [] layouts, Context context) {

        this.layouts = layouts;
        this.ctx = context;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return layouts.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position){
        View view = layoutInflater.inflate(layouts[position], container, false);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {

        View view = (View)object;
        container.removeView(view);
    }

    public void setMyService(VisionARService service) {
        myService = service;
    }
}
