package it.univet.saidemovoice.Instructions_menu;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.IBinder;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import it.univet.saidemovoice.MenuActivity;
import it.univet.saidemovoice.R;
import it.univet.saidemovoice.SpeechRecognizerManagerWakeUp;
import it.univet.visionar.visionarservice.VisionARService;

public class SlideInstrActivity extends AppCompatActivity implements View.OnClickListener, SpeechRecognizerManagerWakeUp.OnResultListener {

    VisionARService VARService = null;
    boolean mBound             = false;

    // Defines callbacks for service binding, passed to bindService()
    private final ServiceConnection binderConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder iBinder) {
            // We've bound to VisionARService, cast the IBinder and get VisionARService instance
            VisionARService.VARBinder binder = (VisionARService.VARBinder) iBinder;
            VARService = binder.getService();

            mBound = true;

            VARService.setVisionARListener(new VisionARService.VisionARListener() {
                @Override
                public void onConnect() {
                    Log.d("VisionAR Listener ", "VISIONAR CONNECTED");
                }

                @Override
                public void onDisconnect() {
                    Log.d("VisionAR Listener ", "VISIONAR DISCONNECTED");
                }
            });
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    // Variabili per il PageAdapter
    private ViewPager mPager;
    private final int[] layouts = {R.layout.activity_slide_intro,
            R.layout.activity_slide_1, R.layout.activity_slide_2, R.layout.activity_slide_3,
            R.layout.activity_slide_4, R.layout.activity_slide_5, R.layout.activity_slide_6};
    private MPagerAdapter mpagerAdapter;
    private LinearLayout dots_layout;
    private ImageView buttonNext, buttonPrev;
    List<Bitmap> instruct_image = new ArrayList<>();

    // Variabili per lo SpeechRecognition
    private static final int PERMISSIONS_REQUEST_RECORD_AUDIO = 1;
    private SpeechRecognizerManagerWakeUp mSpeechRecognizerManager;
    private TextToSpeech textToSpeech;
    private BatteryManager batteryManager;

    // ---------------- 'BEGIN OF PROGRAM' -------------------
    // onCreate, onDestroy, onResume, handleIntent and onNewIntent are always present in application to manage label, intent and others
    // Declaration, assignment of TextView, Button, ...
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slide_main);

        initView();

        initData();
    }

    private void initView() {

        mPager = findViewById(R.id.viewPager);
        dots_layout = findViewById(R.id.dotsLayout);
        buttonNext = findViewById(R.id.btnNext);
        buttonPrev = findViewById(R.id.btnPrev);

        buttonNext.setOnClickListener(this);
        buttonPrev.setOnClickListener(this);

        mpagerAdapter = new MPagerAdapter(layouts, this);
        mPager.setAdapter(mpagerAdapter);
        //With this, only the scroll with button is possible! (Reduce the management of interaction button/swipe)
        mPager.beginFakeDrag(); // DISABLE POSSIBILITY TO SCROLL VIEW WITH SWIPE OF SCREEN!
        mPager.setPageTransformer(true , new ZoomOutPageTransformer());

        createDots(0);

        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                createDots(position);

                if (VARService.WriteImg(instruct_image.get(position), VisionARService.includeBattery.INCLUDE) == VisionARService.writeStatus.SUCCESS){
                    Log.d("@ @ @ ", "writeImage SUCCESS");
                } else Log.d("@ @ @ ", "writeImage FAILED");

                if(position == (layouts.length - 1)){ // At the End of list
                    buttonNext.setImageResource(R.drawable.ic_reload);
                    buttonPrev.setVisibility(View.VISIBLE);
                    buttonPrev.setEnabled(true);
                }else if (position == 0){   // At Begin of list
                    buttonNext.setImageResource(R.drawable.next);
                    buttonPrev.setVisibility(View.INVISIBLE);
                    buttonPrev.setEnabled(false);
                }else{  // Other position of list
                    buttonNext.setImageResource(R.drawable.next);
                    buttonPrev.setVisibility(View.VISIBLE);
                    buttonPrev.setEnabled(true);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        Bitmap image_intro  = BitmapFactory.decodeResource(getResources(), R.drawable.instruction_welcome_image);
        Bitmap image_step_1 = BitmapFactory.decodeResource(getResources(), R.drawable.step_1_inv);
        Bitmap image_step_2 = BitmapFactory.decodeResource(getResources(), R.drawable.step_2_inv);
        Bitmap image_step_3 = BitmapFactory.decodeResource(getResources(), R.drawable.step_3_inv);
        Bitmap image_step_4 = BitmapFactory.decodeResource(getResources(), R.drawable.step_4_inv);
        Bitmap image_step_5 = BitmapFactory.decodeResource(getResources(), R.drawable.step_5_inv);
        Bitmap image_step_6 = BitmapFactory.decodeResource(getResources(), R.drawable.step_6_inv);

        instruct_image.add(image_intro);
        instruct_image.add(image_step_1);
        instruct_image.add(image_step_2);
        instruct_image.add(image_step_3);
        instruct_image.add(image_step_4);
        instruct_image.add(image_step_5);
        instruct_image.add(image_step_6);

        showStartDialog(image_intro);
    }

    private void initData() {

        int permissionCheck = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.RECORD_AUDIO);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, PERMISSIONS_REQUEST_RECORD_AUDIO);
            return;
        }
        // Recognizer initialization is a time-consuming and it involves IO,
        // so we execute it in async task
        mSpeechRecognizerManager = new SpeechRecognizerManagerWakeUp(this);
        mSpeechRecognizerManager.setOnResultListener(this);

        // Text to speech to read Battery Level
        textToSpeech = new TextToSpeech(getApplicationContext(), i -> {
            if(i == TextToSpeech.SUCCESS){
                int lang = textToSpeech.setLanguage(Locale.ENGLISH);    // language selection

                if(lang == TextToSpeech.LANG_MISSING_DATA || lang == TextToSpeech.LANG_NOT_SUPPORTED){
                    Log.d("@ @ @ ", "Not supported language");
                }
            }else Log.d("@ @ @ ", "Initialization failed");
        });

        batteryManager = (BatteryManager) getApplicationContext().getSystemService(BATTERY_SERVICE);
    }

    private void createDots(int current_position) {
        if(dots_layout != null){
            dots_layout.removeAllViews();
        }
        ImageView[] dots = new ImageView[layouts.length];

        for(int i = 0; i < layouts.length; i++){
            dots[i] = new ImageView(this);
            if(i == current_position){
                dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.active_dots));
            }else{
                dots[i].setImageDrawable((ContextCompat.getDrawable(this, R.drawable.inactive_dots)));
            }

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(4, 0, 4, 0);

            dots_layout.addView(dots[i], params);
        }
    }

    private void mStartActivity(Class nextActivity){
        Bitmap menuImage = BitmapFactory.decodeResource(getResources(), R.drawable.first_image_visionar);
        if (VARService.WriteImg(menuImage) == VisionARService.writeStatus.SUCCESS) {
            Log.d("@ @ @ ", "writeImage SUCCESS");
        } else Log.d("@ @ @ ", "writeImage FAILED");

        startActivity(new Intent(SlideInstrActivity.this, nextActivity));
        finish();
    }

    @Override
    public void onClick(View view) {

        if(view == buttonNext){
            loadNextSlide();
        } else if(view == buttonPrev){
            loadPreviousSlide();
        }
    }

    private void loadPreviousSlide(){
        int previous_slide = mPager.getCurrentItem() - 1;

        if(previous_slide >= 0){
            mPager.setCurrentItem(previous_slide);
        }
    }

    private void loadNextSlide(){
        int next_slide = mPager.getCurrentItem() + 1;

        if(next_slide < layouts.length){
            mPager.setCurrentItem(next_slide);
        }else if (next_slide == layouts.length){
            mPager.setCurrentItem(0);
        }
    }

    public void backToMenu(View view) {
        mStartActivity(MenuActivity.class);
    }

    @Override
    public void OnResult(ArrayList<String> commands) {

        for(String command:commands)
        {
            Toast.makeText(this,"You said: " + command, Toast.LENGTH_SHORT).show();

            manageMenuChoice(command);
            return;
        }
    }

    private void manageMenuChoice(String speechInput){
        switch (speechInput.toLowerCase()) {

            case "menu": case "main": {
                mStartActivity(MenuActivity.class);
                Toast.makeText(this, "BACK TO MENU", Toast.LENGTH_SHORT).show();
                CloseActivity();

                break;
            }

            case "next": case "forwards": {
                Toast.makeText(this, "Next Slide", Toast.LENGTH_SHORT).show();

                loadNextSlide();

                break;
            }

            case "back": case "backwards": case "previous": {
                Toast.makeText(this, "Back Slide", Toast.LENGTH_SHORT).show();

                loadPreviousSlide();

                break;
            }

            case "batteria": case "battery": case "livello batteria": case "battery level":{
                SpeakBatteryCharge(batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY));
                break;
            }

            default:{
                break;
            }
        }
    }

    private void SpeakBatteryCharge(int batteryPercentage) {

        String readingString = "Battery level is " + batteryPercentage + " percent";

        textToSpeech.setPitch(1.2f);
        textToSpeech.setSpeechRate(0.7f);
        textToSpeech.speak(readingString, TextToSpeech.QUEUE_FLUSH, null, null);
    }

    private void CloseActivity(){
        mSpeechRecognizerManager.destroy();
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent intent = new Intent(this, VisionARService.class);

        bindService(intent, binderConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();

        mBound = false;
        unbindService(binderConnection);
    }

    private void showStartDialog(Bitmap intro) {

        androidx.appcompat.app.AlertDialog.Builder mBuilder = new androidx.appcompat.app.AlertDialog.Builder(SlideInstrActivity.this);
        mBuilder.setIcon(R.drawable.instructions);
        mBuilder.setTitle("Instructions");
        mBuilder.setMessage("Press OK to enter in\nVisionAR quick guide");
        mBuilder.setPositiveButton("OK", (dialogInterface, i) -> {
            mpagerAdapter.setMyService(VARService);
            if (VARService.WriteImg(intro) == VisionARService.writeStatus.SUCCESS){
                Log.d("@ @ @ ", "writeImage SUCCESS");
            } else Log.d("@ @ @ ", "writeImage FAILED");
            dialogInterface.dismiss();
        });

        AlertDialog versionDialog = mBuilder.create();
        versionDialog.show();
    }
}
