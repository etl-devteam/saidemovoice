package it.univet.saidemovoice;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.Locale;

import it.univet.saidemovoice.Instructions_menu.SlideInstrActivity;
import it.univet.saidemovoice.Settings_menu.SettingsActivity;
import it.univet.saidemovoice.SpeechToText_menu.SpeechToTextActivity;
import it.univet.visionar.visionarservice.VisionARService;

public class MenuActivity extends AppCompatActivity implements View.OnClickListener, SpeechRecognizerManagerWakeUp.OnResultListener {

    CardView card_stt, card_instr, card_sett;

    // SpeechRecognition variable
    private static final int PERMISSIONS_REQUEST_RECORD_AUDIO = 1;
    private SpeechRecognizerManagerWakeUp mSpeechRecognizerManager;
    private TextToSpeech textToSpeech;
    private BatteryManager batteryManager;

    VisionARService VARService = null;
    boolean mBound             = false;

    /** Defines callbacks for service binding, passed to bindService() */
    private final ServiceConnection binderConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder iBinder) {
            // We've bound to VisionARService, cast the IBinder and get VisionARService instance
            VisionARService.VARBinder binder = (VisionARService.VARBinder) iBinder;
            VARService = binder.getService();

            mBound = true;

            VARService.setVisionARListener(new VisionARService.VisionARListener() {
                @Override
                public void onConnect() {
                    Log.d("VisionAR Listener ", "VISIONAR CONNECTED");
                }

                @Override
                public void onDisconnect() {
                    Log.d("VisionAR Listener ", "VISIONAR DISCONNECTED");
                }
            });
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        initView();

        initData();
    }

    private void initView() {
        card_stt = findViewById(R.id.card_STT);
        card_instr = findViewById(R.id.card_Instruct);
        card_sett = findViewById(R.id.card_Settings);

        card_stt.setOnClickListener(this);
        card_instr.setOnClickListener(this);
        card_sett.setOnClickListener(this);
    }

    private void initData() {
        // Check if user has given permission to record audio
        int permissionCheck = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.RECORD_AUDIO);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, PERMISSIONS_REQUEST_RECORD_AUDIO);
            return;
        }
        // Recognizer initialization is a time-consuming and it involves IO,
        // so we execute it in async task
        mSpeechRecognizerManager = new SpeechRecognizerManagerWakeUp(this);
        mSpeechRecognizerManager.setOnResultListener(this);

        // Text to speech to read Battery Level
        textToSpeech = new TextToSpeech(getApplicationContext(), i -> {
            if(i == TextToSpeech.SUCCESS){
                int lang = textToSpeech.setLanguage(Locale.ENGLISH);    // language selection

                if(lang == TextToSpeech.LANG_MISSING_DATA || lang == TextToSpeech.LANG_NOT_SUPPORTED){
                    Log.d("@ @ @ ", "Not supported language");
                }
            }else Log.d("@ @ @ ", "Initialization failed");
        });

        batteryManager = (BatteryManager) getApplicationContext().getSystemService(BATTERY_SERVICE);
    }

    @Override
    public void onClick(View view) {    // Tap the CardView to open different Activities: SpeechToText, Slide and Settings

        if (view == card_stt){
            if(mBound) {
                goToSTTActivity();
            } else Toast.makeText(this, "VisionAR is Not connected!\n", Toast.LENGTH_LONG).show();
        } else if (view == card_instr){
            if(mBound) {
                goToSlideInstrActivity();
            } else Toast.makeText(this, "VisionAR is Not connected!\n", Toast.LENGTH_LONG).show();
        } else if (view == card_sett){
            if(mBound) {
                goToSettingsActivity();
            } else Toast.makeText(this, "VisionAR is Not connected!\n", Toast.LENGTH_LONG).show();
        }
    }

    private void mStartActivity(Class nextActivity){
        startActivity(new Intent(MenuActivity.this, nextActivity));
        finish();
    }

    @Override
    public void OnResult(ArrayList<String> commands) {

        for(String command:commands)
        {
            Toast.makeText(this,"You said: " + command, Toast.LENGTH_SHORT).show();

            manageMenuChoice(command);
            return;
        }
    }

    private void goToSTTActivity() {

        Bitmap bmpWelcomeImage = BitmapFactory.decodeResource(getResources(), R.drawable.speech_recogn_welcome_image);

        if (VARService.WriteImg(bmpWelcomeImage) == VisionARService.writeStatus.SUCCESS) {
            LoadingDialog sTTDialog = new LoadingDialog(MenuActivity.this);
            sTTDialog.startLoadingDialog("Speech To Text");

            Handler handler = new Handler();
            handler.postDelayed(() -> {
                sTTDialog.dismissDialog();
                mStartActivity(SpeechToTextActivity.class);
                CloseActivity();
            }, 3000);
        }
    }

    private void goToSlideInstrActivity() {

        Bitmap bmpWelcomeImage = BitmapFactory.decodeResource(getResources(), R.drawable.instruction_welcome_image);

        if (VARService.WriteImg(bmpWelcomeImage) == VisionARService.writeStatus.SUCCESS) {
            LoadingDialog slideInstrDialog = new LoadingDialog(MenuActivity.this);
            slideInstrDialog.startLoadingDialog("Instructions");

            Handler handler = new Handler();
            handler.postDelayed(() -> {
                slideInstrDialog.dismissDialog();
                mStartActivity(SlideInstrActivity.class);
                CloseActivity();
            }, 3000);
        }
    }

    private void goToSettingsActivity() {

        Bitmap bmpWelcomeImage = BitmapFactory.decodeResource(getResources(), R.drawable.settings_welcome_image);

        if (VARService.WriteImg(bmpWelcomeImage) == VisionARService.writeStatus.SUCCESS) {

            LoadingDialog settingsDialog = new LoadingDialog(MenuActivity.this);
            settingsDialog.startLoadingDialog("Settings");

            Handler handler = new Handler();
            handler.postDelayed(() -> {
                settingsDialog.dismissDialog();
                mStartActivity(SettingsActivity.class);
                CloseActivity();
            }, 3000);
        }
    }

    private void manageMenuChoice(String speechInput){
        switch (speechInput.toLowerCase()) {

            case "speech": case "dictate": {
                if(mBound) {
                    goToSTTActivity();
                } else Toast.makeText(this, "VisionAR is Not connected!\n", Toast.LENGTH_LONG).show();
                break;
            }

            case "manual": case "manuals": case "instruction": case "instructions": case "slide instructions": {
                if(mBound) {
                    goToSlideInstrActivity();
                } else Toast.makeText(this, "VisionAR is Not connected!\n", Toast.LENGTH_LONG).show();
                break;
            }

            case "settings": case "setting": {
                if(mBound) {
                    goToSettingsActivity();
                } else Toast.makeText(this, "VisionAR is Not connected!\n", Toast.LENGTH_LONG).show();
                break;
            }

            case "batteria": case "battery": case "livello batteria": case "battery level":{

                SpeakBatteryCharge(batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY));

                break;
            }

            default:{
                break;
            }
        }
    }

    private void SpeakBatteryCharge(int batteryPercentage) {

        String readingString = "Battery level is " + batteryPercentage + " percent";

        textToSpeech.setPitch(1.2f);
        textToSpeech.setSpeechRate(0.7f);
        textToSpeech.speak(readingString, TextToSpeech.QUEUE_FLUSH, null, null);
    }

    private void CloseActivity(){
        finish();
        mSpeechRecognizerManager.destroy();
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent intent = new Intent(this, VisionARService.class);

        bindService(intent, binderConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();

        mBound = false;
        unbindService(binderConnection);
    }
}
