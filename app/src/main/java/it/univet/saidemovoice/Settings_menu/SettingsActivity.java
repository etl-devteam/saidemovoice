package it.univet.saidemovoice.Settings_menu;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.SeekBar;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Timer;
import java.util.TimerTask;

import it.univet.saidemovoice.MenuActivity;
import it.univet.saidemovoice.R;
import it.univet.visionar.visionarservice.VisionARService;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {

    VisionARService VARService = null;
    boolean mBound             = false;

    // Polling to visualize Layout directly on VisionAR display
    private static final int UPDATE_VIEW_TIMER_POLLING = 300;
    private final Handler updateViewHandler = new Handler();
    private Timer updateViewTimer;

    private Button back_btn_settings, vibration_btn;
    private NumberPicker numberPickerVib;
    private String[] numbers_picker;

    /** Defines callbacks for service binding, passed to bindService() */
    private final ServiceConnection binderConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder iBinder) {
            // We've bound to VisionARService, cast the IBinder and get VisionARService instance
            VisionARService.VARBinder binder = (VisionARService.VARBinder) iBinder;
            VARService = binder.getService();

            mBound = true;

            VARService.setTouchpadListener(ev -> {
                if (ev == VisionARService.touchpadEvent.SINGLE_TAP) {
                    Log.d("act list ", "Single Tap");
                    runOnUiThread(() -> Toast.makeText(SettingsActivity.this, "Touchpad: SINGLE TAP", Toast.LENGTH_SHORT).show());
                } else if (ev == VisionARService.touchpadEvent.DOUBLE_TAP) {
                    Log.d("act list ", "Double Tap");
                    runOnUiThread(() -> Toast.makeText(SettingsActivity.this, "Touchpad: DOUBLE TAP", Toast.LENGTH_SHORT).show());
                } else if (ev == VisionARService.touchpadEvent.SWIPE_DOWN) {
                    Log.d("act list ", "Swipe Down");
                    runOnUiThread(() -> Toast.makeText(SettingsActivity.this, "Touchpad: SWIPE DOWN", Toast.LENGTH_SHORT).show());
                } else {  // VisionARService.touchpadEvent.SWIPE_UP
                    Log.d("act list ", "Swipe Up");
                    runOnUiThread(() -> Toast.makeText(SettingsActivity.this, "Touchpad: SWIPE UP", Toast.LENGTH_SHORT).show());
                }
            });

            VARService.StartTouchpad();

            VARService.setVisionARListener(new VisionARService.VisionARListener() {
                @Override
                public void onConnect() {
                    Log.d("VisionAR Listener ", "VISIONAR CONNECTED");
                }

                @Override
                public void onDisconnect() {
                    Log.d("VisionAR Listener ", "VISIONAR DISCONNECTED");
                }
            });
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        initView();

        initData();
    }

    private void initView() {
        back_btn_settings = findViewById(R.id.back_button_setting);
        vibration_btn     = findViewById(R.id.vibrate_set);

        back_btn_settings.setOnClickListener(this);
        vibration_btn.setOnClickListener(this);

        Bitmap image_intro = BitmapFactory.decodeResource(getResources(), R.drawable.settings_welcome_image);
        showStartDialog(image_intro);

        // SeekBar to adjust contrast and brightness of glasses
        SeekBar contrBar = findViewById(R.id.contrSeekBar);
        contrBar.setOnSeekBarChangeListener(contrErGlassChangeListener);
        SeekBar brightBar = findViewById(R.id.brightSeekBar);
        brightBar.setOnSeekBarChangeListener(brightErGlassChangeListener);

        // Picker for vibration
        int minValue_picker = 100;
        int maxValue_picker = 3100;
        int step_picker = 100;

        numbers_picker = new String[maxValue_picker / minValue_picker];
        for (int i = 0; i < numbers_picker.length; i++) {
            numbers_picker[i] = String.valueOf(step_picker + (i * step_picker));
        }
        numberPickerVib = findViewById(R.id.pickerVibration);
        numberPickerVib.setDisplayedValues(numbers_picker);
        numberPickerVib.setMaxValue(29);
        numberPickerVib.setMinValue(0);
    }

    private void initData() {
    }

    SeekBar.OnSeekBarChangeListener contrErGlassChangeListener = new SeekBar.OnSeekBarChangeListener() {
        int myProgress_contrast = 120;

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            myProgress_contrast = progress;
        }

        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        public void onStopTrackingTouch(SeekBar seekBar) {
            if (VARService.Contrast(myProgress_contrast) == VisionARService.writeStatus.SUCCESS) {
                Log.d("@ @ @ ", "Contrast SUCCESS");
            } else Log.d("@ @ @ ", "Contrast FAILED");
            Toast.makeText(SettingsActivity.this, "Contrast value: " + myProgress_contrast, Toast.LENGTH_SHORT).show();
        }
    };

    SeekBar.OnSeekBarChangeListener brightErGlassChangeListener = new SeekBar.OnSeekBarChangeListener() {
        int myProgress_brightness = 120;

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            myProgress_brightness = progress;
        }

        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        public void onStopTrackingTouch(SeekBar seekBar) {
            if (VARService.Brightness(myProgress_brightness) == VisionARService.writeStatus.SUCCESS){
                Log.d("@ @ @ ", "Brightness SUCCESS");
            } else Log.d("@ @ @ ", "Brightness FAILED");
            Toast.makeText(SettingsActivity.this, "Brightness value: " + myProgress_brightness, Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    protected void onStart() {
        super.onStart();

        Intent intent = new Intent(this, it.univet.visionar.visionarservice.VisionARService.class);

        bindService(intent, binderConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();

        try {
            updateViewTimer.cancel();
        } catch (Exception e) {
            Log.d("@ @ @ ", "Exception: " + e);
        }

        if (mBound) VARService.StopTouchpad();

        mBound = false;
        unbindService(binderConnection);
    }

    @Override
    public void onClick(View view) {

        view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY, HapticFeedbackConstants.FLAG_IGNORE_GLOBAL_SETTING);

        if (view == back_btn_settings){

            try {
                updateViewTimer.cancel();
            } catch (Exception e) {
                Log.d("@ @ @ ", "Exception: " + e);
            }

            Bitmap menuImage = BitmapFactory.decodeResource(getResources(), R.drawable.first_image_visionar);
            if (VARService.WriteImg(menuImage) == VisionARService.writeStatus.SUCCESS) {
                Log.d("@ @ @ ", "writeImage SUCCESS");
            } else Log.d("@ @ @ ", "writeImage FAILED");

            startActivity(new Intent(SettingsActivity.this, MenuActivity.class));
            finish();
        } else if (view == vibration_btn){
            if (VARService.Haptic(Integer.parseInt(numbers_picker[numberPickerVib.getValue()])) == VisionARService.writeStatus.SUCCESS){
                Log.d("@ @ @ ", "Haptic SUCCESS");
            } else Log.d("@ @ @ ", "Haptic FAILED");
        }
    }

    private void showStartDialog(Bitmap intro) {

        androidx.appcompat.app.AlertDialog.Builder mBuilder =
                new androidx.appcompat.app.AlertDialog.Builder(it.univet.saidemovoice.Settings_menu.SettingsActivity.this);

        mBuilder.setIcon(R.drawable.bright_contr_setting);
        mBuilder.setTitle("Settings");
        mBuilder.setMessage("Press OK to configure Display");

        mBuilder.setPositiveButton("OK", (dialogInterface, i) -> {
            if (VARService.Contrast(125) == VisionARService.writeStatus.SUCCESS) {
                Log.d("@ @ @ ", "Contrast SUCCESS");
            } else Log.d("@ @ @ ", "Contrast FAILED");
            if (VARService.Brightness(125) == VisionARService.writeStatus.SUCCESS) {
                Log.d("@ @ @ ", "Brightness SUCCESS");
            } else Log.d("@ @ @ ", "Brightness FAILED");

            updateViewTimer = new Timer();
            updateViewTimer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    updateViewHandler.post(() -> {
                        try{
                            Bitmap updateViewImage = LayoutToBitmap(R.layout.activity_settings, R.id.settingLayout);

                            if (VARService.WriteImg(updateViewImage, VisionARService.includeBattery.INCLUDE, VisionARService.invertBitmap.NO_INVERT) == VisionARService.writeStatus.SUCCESS) {
                                Log.d("@ @ @ ", "writeImage SUCCESS");
                            } else Log.d("@ @ @ ", "writeImage FAILED");

                        } catch(Exception e) {
                            Log.d("@ @ @ ", "Exception: " + e);
                        }
                    });
                }
            }, 0, UPDATE_VIEW_TIMER_POLLING);

            dialogInterface.dismiss();
        });

        AlertDialog versionDialog = mBuilder.create();
        versionDialog.show();
    }

    // Method to save into Internal Storage a Bitmap file
    /*private String saveToInternalStorage(Bitmap bitmapImage) {
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath = new File(directory,"savedImage_1.jpg");
        Log.d("@ @ @ ", "Directory: " + directory);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);

            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);

            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return directory.getAbsolutePath();
    }*/

    // ---------------- 'BACKGROUND ANIMATION ON GLASSES' -------------------

    // activityLayout is the current activity, selectedView is the interested Layout View.
    // Layout View is converted into Bitmap in order to be shown on VisionAR display
    private Bitmap LayoutToBitmap(int activityLayout, int selectedView) {
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
        ViewGroup layout = (ViewGroup) inflater.inflate(activityLayout, null);
        layout.setDrawingCacheEnabled(true);

        return getBitmapFromView(this.getWindow().findViewById(selectedView));
    }

    // It is called from TableLayout_to_Bitmap and it is used to draw layout on canvas in order to write layout itself on returned bitmap
    public static Bitmap getBitmapFromView(View view) {
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(),Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable!=null){
            bgDrawable.draw(canvas);
        } else{
            canvas.drawColor(Color.WHITE);
        }
        view.draw(canvas);

        return returnedBitmap;
    }

    // ---------------- END 'BACKGROUND ANIMATION ON GLASSES' -------------------
}