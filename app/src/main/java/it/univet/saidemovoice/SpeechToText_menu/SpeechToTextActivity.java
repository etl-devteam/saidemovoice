package it.univet.saidemovoice.SpeechToText_menu;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.IBinder;
import android.speech.RecognizerIntent;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Locale;

import it.univet.saidemovoice.MenuActivity;
import it.univet.saidemovoice.R;
import it.univet.visionar.visionarservice.VisionARService;

public class SpeechToTextActivity  extends AppCompatActivity implements View.OnClickListener {

    VisionARService VARService = null;
    boolean mBound             = false;

    private static final int PERMISSIONS_REQUEST_RECORD_AUDIO = 1;

    private Button back_btn_stt;
    private TextView speechResultText;
    private ImageView microphone_icon;

    /** Defines callbacks for service binding, passed to bindService() */
    private final ServiceConnection binderConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder iBinder) {
            // We've bound to VisionARService, cast the IBinder and get VisionARService instance
            VisionARService.VARBinder binder = (VisionARService.VARBinder) iBinder;
            VARService = binder.getService();

            mBound = true;

            VARService.setVisionARListener(new VisionARService.VisionARListener() {
                @Override
                public void onConnect() {
                    Log.d("VisionAR Listener ", "VISIONAR CONNECTED");
                }

                @Override
                public void onDisconnect() {
                    Log.d("VisionAR Listener ", "VISIONAR DISCONNECTED");
                }
            });
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stt);

        initView();

        requestPermission();

        initData();
    }

    private void initView() {
        back_btn_stt = findViewById(R.id.back_button_stt);
        speechResultText = findViewById(R.id.speechResult);
        microphone_icon = findViewById(R.id.mic_icon);

        back_btn_stt.setOnClickListener(this);
        microphone_icon.setOnClickListener(this);

        Bitmap image_intro = BitmapFactory.decodeResource(getResources(), R.drawable.speech_recogn_welcome_image);
        showStartDialog(image_intro);

        speechResultText.setText("");   // clear the Text field
        speechResultText.setMovementMethod(new ScrollingMovementMethod());

        if(!hasRecordAudioPermission()){
            microphone_icon.getDrawable().setColorFilter(this.getColor(R.color.my_red), PorterDuff.Mode.MULTIPLY);
        } else{
            microphone_icon.getDrawable().setColorFilter(this.getColor(R.color.my_green), PorterDuff.Mode.MULTIPLY);
        }
    }

    private void initData() {
        AudioManager audioManager = (AudioManager) getSystemService((Context.AUDIO_SERVICE));

        audioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, 0,0);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent intent = new Intent(this, it.univet.visionar.visionarservice.VisionARService.class);

        bindService(intent, binderConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();

        mBound = false;
        unbindService(binderConnection);
    }

    @Override
    public void onClick(View view) {

        if (view == back_btn_stt){
            Bitmap menuImage = BitmapFactory.decodeResource(getResources(), R.drawable.first_image_visionar);
            if (VARService.WriteImg(menuImage) == VisionARService.writeStatus.SUCCESS) {
                Log.d("@ @ @ ", "writeImage SUCCESS");
            } else Log.d("@ @ @ ", "writeImage FAILED");

            startActivity(new Intent(SpeechToTextActivity.this, MenuActivity.class));
            finish();
        } else if (view == microphone_icon){
            Toast.makeText(SpeechToTextActivity.this, "Permission Audio: " + hasRecordAudioPermission(), Toast.LENGTH_SHORT).show();
            if (hasRecordAudioPermission()) {
                Speak();
            }
        }
    }

    private void showStartDialog(Bitmap intro) {

        androidx.appcompat.app.AlertDialog.Builder mBuilder = new androidx.appcompat.app.AlertDialog.Builder(SpeechToTextActivity.this);
        mBuilder.setIcon(R.drawable.speech_to_text);
        mBuilder.setTitle("Speech To Text");
        mBuilder.setMessage("Press OK to enter in Speech Recognition");
        mBuilder.setPositiveButton("OK", (dialogInterface, i) -> {
            if (VARService.WriteImg(intro) == VisionARService.writeStatus.SUCCESS){
                Log.d("@ @ @ ", "writeImage SUCCESS");
            } else Log.d("@ @ @ ", "writeImage FAILED");
            dialogInterface.dismiss();
        });

        AlertDialog versionDialog = mBuilder.create();
        versionDialog.show();
    }

    private boolean hasRecordAudioPermission() {
        return checkSelfPermission(Manifest.permission.RECORD_AUDIO) ==
                PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        if (!hasRecordAudioPermission()) {
            requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO}, PERMISSIONS_REQUEST_RECORD_AUDIO);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERMISSIONS_REQUEST_RECORD_AUDIO) {
            boolean granted = grantResults.length > 0 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED;
            if (granted)
                Toast.makeText(this, "Record Audio granted", Toast.LENGTH_SHORT).show();
        }
    }

    private void Speak() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Send information to VisionAR");
        speechResultText.setText(R.string.listening_str);

        try {
            startActivityForResult(intent, PERMISSIONS_REQUEST_RECORD_AUDIO);
        } catch (Exception e) {
            Log.d("@ @ @ ", "Error: " + e);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PERMISSIONS_REQUEST_RECORD_AUDIO) {
            if (resultCode == RESULT_OK && null != data) {
                ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                String resultFromSpeech = result.get(0);
                //Toast.makeText(this, "Result: " + resultFromSpeech, Toast.LENGTH_SHORT).show();
                speechResultText.setText(String.format("Result: %s", resultFromSpeech));

                if (VARService.WriteText(resultFromSpeech, VisionARService.includeBattery.INCLUDE) == VisionARService.writeStatus.SUCCESS) {
                    Log.d("@ @ @ ", "writeText SUCCESS");
                } else Log.d("@ @ @ ", "writeText FAILED");
            }
        }
    }
}
